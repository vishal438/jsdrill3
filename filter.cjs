function filter(elements,callback){
   let ans=[]
   for(let item=0;item<elements.length;item++){
    if(callback(elements[item],item,elements) === true){
        ans.push(elements[item])
    }
   }
   return ans;
}
module.exports=filter;