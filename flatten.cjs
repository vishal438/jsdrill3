function flatten(elements,depth=1){
    const ans=[]
    function flattenHelping(elements,depth,start=0){
        for(let item=0;item<elements.length;item++){
            if(Array.isArray(elements[item]) && depth>0 && start<depth){
               flattenHelping(elements[item],depth,start+1)
            }
            else{
                if(elements[item]!==undefined){
                    ans.push(elements[item])
                }
                
            }
        }
      }
      flattenHelping(elements,depth)
      return ans
    

}
module.exports=flatten