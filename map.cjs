function map(elements,callback){
   const ans=[]
   for(let item=0;item<elements.length;item++){
       ans.push(callback(elements[item],item,elements))
   }
   return ans
}
module.exports=map;