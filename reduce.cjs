function reduce(elements,callback,startingValue){
  
    let accumulator=startingValue

    if(accumulator===undefined){
          if(elements.length===0){
            throw new TypeError("cannot apply reduce on empty array")
          }
          accumulator=elements[0]
          for(let item=1;item<elements.length;item++){
            accumulator=callback(accumulator,elements[item],item,elements)
          }
    }
    else{
        for(let item=0;item<elements.length;item++){
            accumulator=callback(accumulator,elements[item],item,elements)
        }
    }
    return accumulator

}
module.exports=reduce;